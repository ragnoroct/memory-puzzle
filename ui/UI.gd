extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
    # Called when the node is added to the scene for the first time.
    # Initialization here
    pass

func _process(delta):
    $Score/Turns.text = str(global.turns) + " : TURNS"
    $Score/Score.text = str(global.matched_tiles_count) + " : MATCHES"
    
func _on_StartButton_pressed():
    get_node("/root/Main").start_game()
    $Menu/StartButton.visible = false
    $Menu/ClickSound.play()


func _on_ReplayButton_pressed():
    get_node("/root/Main").replay_game()
    $Menu/ReplayButton.visible = false
    $Menu/ClickSound.play()
