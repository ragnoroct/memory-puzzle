tool
extends Sprite

export(String) var button_text setget set_button_text

var label

func _ready():
    self.label = Label.new()
    self.label.text = button_text
    label.align = HALIGN_CENTER
    add_child(label)
    center_label()
    
    label.connect("item_rect_changed", self, "center_label")
    

func center_label():
    if label != null:
        label.set_size(Vector2(1, 1))
        print(self.position)
        print("Label pos:", label.rect_position)
        print("Label size:", label.get_combined_minimum_size())
        var center_position = Vector2(texture.get_width() / 2, texture.get_height()) / 2
        center_position -= (label.get_size() / 2) + Vector2(1,1)
        label.rect_position = center_position

func set_button_text(value):
    button_text = value
    if self.label != null:
        self.label.set_text(button_text)