tool
extends EditorImportPlugin

enum Presets { PRESET_DEFAULT }

func get_importer_name():
    return "ragnoroct.bmfont_importer"

func get_visible_name():
    return "BitmapFont Importer"

func get_recognized_extensions():
    return ["fntd"]

func get_save_extension():
    return "tres"

func get_resource_type():
    return "BitmapFont"

func get_preset_count():
    return 1

func get_preset_name(preset):
    match preset:
        PRESET_DEFAULT: return "Default"
        _ : return "Unknown"

func get_import_options(preset):
    match preset:
        _: return []

func get_option_visibility(option, options):
    return true

const CHAR_KEYS = {
    KEY = "id",
    X = "x",
    Y = "y",
    WIDTH = "width",
    HEIGHT = "height"
}

class FontData:
    var image_path = ""
    var char_data = []

func import(source_file, save_path, options, r_platform_variants, r_gen_files):
    var file = File.new()
    var err = file.open(source_file, File.READ)
    if err != OK:
        return err

    #parse text data
    var font_data = parse_font_data_text(file)
    file.close()
    
    #load texture
    var texture_path = source_file.get_base_dir() + "/" + font_data.image_path
    var texture = load(texture_path)

    var bmfont = BitmapFont.new()
    bmfont.add_texture(texture)
    bmfont.set_height(16)
    for char_def in font_data.char_data:
        bmfont.add_char(
            int(char_def["id"]), 
            0, 
            Rect2(
                int(char_def["x"]), 
                int(char_def["y"]), 
                int(char_def["width"]), 
                int(char_def["height"])
        ))
    return ResourceSaver.save("%s.%s" % [save_path, get_save_extension()], bmfont)

func parse_font_data_text(file):
    var font_data = FontData.new()
    var line
    var fields
    while true:
        line = file.get_line()
        if line == "":
            break
            
        # Remove double spaces
        var regex = RegEx.new()
        regex.compile("\\s+")
        line = regex.sub(line, " ", true)

        fields = line.split(" ", true)
        match fields[0]:
            "char":
                var char_dict = array_to_dict(fields)
                font_data.char_data.append(char_dict)
            "page":
                var info_dict = array_to_dict(fields)
                font_data.image_path = info_dict["file"].replace('"', '')
    return font_data
    
func array_to_dict(array):
    var dict = {}
    for field in array:
        var key_value_array = field.split("=")
        if key_value_array.size() == 2:
            var key = key_value_array[0]
            var value = key_value_array[1]
            dict[key] = value
    return dict