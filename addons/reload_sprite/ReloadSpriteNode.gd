tool
extends EditorPlugin

func _enter_tree():
    add_custom_type("ReloadSprite", "Sprite", preload("ReloadSprite.gd"), preload("icon.png"))
    print("add autoload")

func _exit_tree():
    remove_custom_type("ReloadSprite")
    print("remove autoload")