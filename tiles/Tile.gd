extends Node2D

const TileImage = preload("res://tiles/TileImage.gd")

const FRAME_SPEED_SECONDS = .01
const FRAMES = 8
const FLIP_TIMER = 1

enum STATES {
    back,
    front,
    back_to_front,
    front_to_back
}

var current_frame
var state
var sprite
var image
var back_image
var type
var frame_timer
var location

var tile_image

#Public
var locked
var matched


func init(type, image, back_image, location):
    self.type = type
    self.image = image
    self.back_image = back_image
    self.location = location
    self.reset_tile_state()

func reset_tile_state():
    frame_timer = Timer.new()
    frame_timer.wait_time = FRAME_SPEED_SECONDS
    frame_timer.connect("timeout", self, "_animate_timout")
    state = STATES.back
    locked = false
    matched = false

    tile_image = TileImage.new()
    tile_image.init(image)
    tile_image.show_overlay_unmatched()
    flip_to_back()

func _ready():
    sprite = Sprite.new()
    sprite.texture = back_image
#    add_child(sprite)
    add_child(self.tile_image)
    add_child(frame_timer)

# ---- Public Methods ----
func click():
    if locked == false and state == STATES.back and matched == false:
        $FlipSound.play()
        flip_tile()
    
func lock_tile():
    locked = true
    
func match_tile():
    matched = true
    tile_image.show_overlay_matched()
    if global.is_all_tiles_matched() == false:
        $MatchSound.play()
    
    
func flip_tile():
    frame_timer.start()
    global.flipping_tile_count += 1
    current_frame = 0
    if state == STATES.back:
        state = STATES.back_to_front
    else:
        state = STATES.front_to_back

func flip_to_back():
    if state == STATES.front:
        frame_timer.start()
        global.flipping_tile_count += 1
        current_frame = 0
        state = STATES.front_to_back

# ---- Private Methods ----
func _animate_timout():
    current_frame += 1
    tile_image.update(self.state, self.current_frame)
    if current_frame == FRAMES:
        state = STATES.back if state == STATES.front_to_back else STATES.front
        frame_timer.stop()
        global.flipping_tile_count -= 1
        if state == STATES.front:
            global.add_tile(self)