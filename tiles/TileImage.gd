extends Node2D

const Tile = preload("res://tiles/Tile.gd")
const ReloadSprite = preload("res://addons/reload_sprite/ReloadSprite.gd")

#sprites
var front_image
var back_image
var back_overlay_image
var unmatched_overlay_image
var matched_overlay_image

func init(front_image_texture):
    self.front_image = ReloadSprite.new()
    self.front_image.set_texture(front_image_texture)
    self.back_image = ReloadSprite.new()
    self.back_image.set_texture(preload("res://tiles/images/back.png"))
    self.unmatched_overlay_image = ReloadSprite.new()
    self.unmatched_overlay_image.set_texture(preload("res://tiles/images/unmatched_overlay.png"))
    self.matched_overlay_image = ReloadSprite.new()
    self.matched_overlay_image.set_texture(preload("res://tiles/images/matched_overlay.png"))
    self.back_overlay_image = ReloadSprite.new()
    self.back_overlay_image.texture = preload("res://tiles/images/back_overlay.png")
#    var thing = ReloadSprite.new()
#    thing.texture = self.back_overlay_image.texture

func _ready():
    add_child(self.front_image)
    add_child(self.back_image)
    add_child(self.unmatched_overlay_image)
    add_child(self.matched_overlay_image)
    add_child(self.back_overlay_image)
    hide_all_images()
    self.back_image.show()
    self.back_overlay_image.show()
    
# ---- Public Methods ----
func update(state, current_frame):
    var closing
    var flip_percent = current_frame / float(Tile.FRAMES)
    if flip_percent <= .5:
        flip_percent = 1 - (flip_percent * 2)
        closing = true
    else:
        flip_percent = (flip_percent - .5) * 2
        closing = false
    self.scale = Vector2(flip_percent, 1)

    hide_all_images()
    match state:
        Tile.STATES.back:
            self.back_image.show()
            self.back_overlay_image.show()
        Tile.STATES.front:
            self.front_image.show()
            self.unmatched_overlay_image.show()
        Tile.STATES.back_to_front:
            if closing:
                self.back_image.show()
                self.back_overlay_image.show()
            else:
                self.front_image.show()
                self.unmatched_overlay_image.show()
        Tile.STATES.front_to_back:
            if closing:
                self.unmatched_overlay_image.show()
                self.front_image.show()
            else:
                self.back_overlay_image.show()
                self.back_image.show()
                
func show_overlay_matched():
    self.matched_overlay_image.show()
    self.unmatched_overlay_image.hide()
    
func show_overlay_unmatched():
    self.matched_overlay_image.hide()
    self.unmatched_overlay_image.hide()
    
# ---- Private Methods ----
func hide_all_images():
    self.front_image.hide()
    self.back_image.hide()
    self.unmatched_overlay_image.hide()
    self.matched_overlay_image.hide()
    self.back_overlay_image.hide()
