import os
import subprocess
from pathlib import Path
from glob import glob
import re
import shutil

out = subprocess.check_output("git rev-parse --show-toplevel").decode('utf-8').strip('\n')
rootPath = Path(out)
buildDir = rootPath / "build"

windowsDir = buildDir / "windows"
htmlDir = buildDir / "html"
linuxDir = buildDir / "linux"
macDir = buildDir / "mac"

#Setup fs
if buildDir.exists():
    shutil.rmtree(buildDir)
    while os.path.exists(buildDir): # check if it exists
        pass

os.mkdir(buildDir)
os.mkdir(windowsDir)
os.mkdir(htmlDir)
os.mkdir(linuxDir)
os.mkdir(macDir)

#Export html
print("> exporting html")
cmd = f'godot --no-window --quiet --path "{rootPath}" --export "HTML5" "{ htmlDir / "index.html"}"'
os.system(cmd)
shutil.make_archive("memory-game-html", 'zip', htmlDir)
shutil.move("memory-game-html.zip", buildDir)

#Export windows (Windows Desktop)
print("> exporting windows")
cmd = f'godot --no-window --quiet --path "{rootPath}" --export "Windows Desktop" "{ windowsDir / "memory-game.exe"}"'
os.system(cmd)
shutil.make_archive("memory-game-windows", 'zip', windowsDir)
shutil.move("memory-game-windows.zip", buildDir)

#Export linux (Linux/X11)
print("> exporting linux")
cmd = f'godot --no-window --quiet --path "{rootPath}" --export "Linux/X11" "{ linuxDir / "memory-game"}"'
os.system(cmd)
shutil.make_archive("memory-game-linux", 'gztar', linuxDir)
shutil.move("memory-game-linux.tar.gz", buildDir)

#Export mac (Mac OSX)
print("> exporting osx")
cmd = f'godot --no-window --quiet --path "{rootPath}" --export "Mac OSX" "{ buildDir / "memory-game-osx.zip"}"' #automatically zips
os.system(cmd)
