import os
import subprocess
from pathlib import Path
from glob import glob
import re

out = subprocess.check_output("git rev-parse --show-toplevel").decode('utf-8').strip('\n')
rootPath = Path(out)
for root, dirs, files in os.walk(rootPath):
    files = [f for f in files if f.endswith(".import")]   #remove .import files
    dirs[:] = [d for d in dirs if not d[0] == '.']  #remove hidden folders
    for f in files:
        strippedImport = re.sub(r'.import$', '', f)
        importTarget = Path(f"{root}\\{strippedImport}")
        if importTarget.exists() == False:
            print(f"Deleting orphaned import: {f}")
            os.remove(Path(root) / Path(f))