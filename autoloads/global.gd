extends Node

var matched_tiles_count = 0
var turns = 0
var clicked_tiles = []
var all_tiles
var flipping_tile_count = 0 setget set_flipping_tile_count

var game_mode

enum GAME_MODE {
    MENU,
    PLAY   
}

func set_flipping_tile_count(value):
    flipping_tile_count = value
    if flipping_tile_count == 0 and game_mode == GAME_MODE.MENU:
        game_mode = GAME_MODE.PLAY
        get_node("/root/Main").setup_tiles() #reshuffle board

func get_turns():
    return turns

func add_tile(tile):
    clicked_tiles.append(tile)
    if clicked_tiles.size() == 2:
        if clicked_tiles[0].type == clicked_tiles[1].type:
            matched_tiles_count += 1
            clicked_tiles[0].match_tile()
            clicked_tiles[1].match_tile()
        else:
            lock_all_tiles()
        turns += 1
        clicked_tiles = []
    check_end_game()

func check_end_game():
    var main = get_node('/root/Main')
    if is_all_tiles_matched():
        main.end_game()        
        
func is_all_tiles_matched():
    var main = get_node('/root/Main')
    if matched_tiles_count == (main.BOARD_WIDTH * main.BOARD_HEIGHT) / 2:
        return true
    else:
        return false
        
func match_tile(tile_to_match):
    var tile
    for i in range(clicked_tiles.size()):
        tile = clicked_tiles[i]
        if tile.type == tile_to_match.type and tile != tile_to_match:
            tile.lock_tile()
            tile_to_match.lock_tile()
            clicked_tiles.erase(tile)
            clicked_tiles.erase(tile_to_match)
            return true
    return false

func lock_all_tiles():
    for row in all_tiles:
        for tile in row:
            tile.locked = true
    var timer = Timer.new()
    timer.wait_time = preload("res://tiles/Tile.gd").FLIP_TIMER
    timer.one_shot = true
    timer.connect("timeout", self, "unlock_all_tiles")
    timer.start()
    add_child(timer)
    
func unlock_all_tiles():
    for row in all_tiles:
        for tile in row:
            tile.locked = false
            if tile.matched == false:
                tile.flip_to_back()