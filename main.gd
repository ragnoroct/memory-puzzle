extends Node2D

const BOARD_WIDTH = 6
const BOARD_HEIGHT = 6
const TILE_SIZE = 32
const TILE_PADDING = 2
const Util = preload("res://Util.gd")

var TILE_LIST

const Util = preload("res://Util.gd")

var tiles
var x_offset
var y_offset

func _ready():
    var image_list = Util.list_files_in_directory("res://tiles/images/shapes")    
    TILE_LIST = []
    for image in image_list:
        if image.ends_with(".png.import"):
            var tile_type = image.split(".")[0]
            TILE_LIST.append(tile_type)
    setup_tiles()
    
    #Set starting game mode
    global.game_mode = global.GAME_MODE.MENU
    get_node("UI/Menu/ReplayButton").visible = false
    
func start_game():
    global.game_mode = global.GAME_MODE.PLAY
    
func replay_game():
    for tile_row in tiles:
        for tile in tile_row:
            tile.tile_image.show_overlay_unmatched()
            tile.flip_to_back()
            tile.locked = false
            tile.matched = false
    global.matched_tiles_count = 0
    global.clicked_tiles = []
    global.turns = 0

func end_game():
    $WonGameSound.play()
    get_node("UI/Menu/ReplayButton").visible = true
    global.game_mode = global.GAME_MODE.MENU

func _unhandled_input(event):
   if event is InputEventMouseButton:
        if event.pressed and global.game_mode == global.GAME_MODE.PLAY:
            var pos_clicked = event.position - Vector2(x_offset, y_offset)
            var tile_with_padding = Vector2(TILE_SIZE + TILE_PADDING, TILE_SIZE + TILE_PADDING)
            var square_location = (pos_clicked / tile_with_padding).floor()
            if square_location >= Vector2(0,0):
                var relative_tile_click = pos_clicked - (square_location * (TILE_SIZE + TILE_PADDING))
                if square_location.x < BOARD_WIDTH and square_location.y < BOARD_HEIGHT:
                    if relative_tile_click.x > TILE_PADDING and relative_tile_click.y > TILE_PADDING:
                        tiles[square_location.x][square_location.y].click()
        
func setup_tiles():
    for child in $Shapes.get_children():
        child.queue_free()
    
    var tile_res = load("res://tiles/Tile.tscn")
    var tile_list = []
    
    #Calculate starting position
    var board_width = (TILE_SIZE + TILE_PADDING) * BOARD_WIDTH
    var board_height = (TILE_SIZE + TILE_PADDING) * BOARD_HEIGHT
    x_offset = (get_viewport_rect().size.x / 2) - (board_width / 2)
    y_offset = (get_viewport_rect().size.y / 2) - (board_height / 2)
    
    #Load all the images
    var back_image = load("res://tiles/images/back.png")
    var tile_images = {}
    for tile_type in TILE_LIST:
        var path = "res://tiles/images/shapes/" + tile_type + ".png"
        var image = load(path)
        tile_images[tile_type] = image
    
    var type_bag = create_type_grab_bag()
    tiles = []
    for x in range(BOARD_WIDTH):
        tiles.append([])
        for y in range(BOARD_HEIGHT):
#            var tile_type = TILE_LIST[randi()%TILE_LIST.size()+0]
            var tile_type = type_bag.pop_back()
            var tile_image = tile_images[tile_type]
            var tile = tile_res.instance()
            tile.init(tile_type, tile_image, back_image, Vector2(x, y))
            tiles[x].append(tile)
            var x_pos = (x * TILE_SIZE) + ((x + 1) * TILE_PADDING) + (TILE_SIZE / 2) + x_offset
            var y_pos = (y * TILE_SIZE) + ((y + 1) * TILE_PADDING) + (TILE_SIZE / 2) + y_offset
            tile.position = Vector2(x_pos, y_pos)
            $Shapes.add_child(tile)
    global.all_tiles = tiles

func create_type_grab_bag():
    var bag = []
    var pairs_per_type
    var pairs_per_board

    #Calculate the number of pairs each type will have
    pairs_per_board  = (BOARD_HEIGHT * BOARD_WIDTH) / 2
    pairs_per_type = pairs_per_board / TILE_LIST.size()
    
    #Create bag of types
    for type in TILE_LIST:
        #Append pairs_per_type pairs
        for i in range(pairs_per_type):
            bag.append(type)
            bag.append(type)
    
    #Add types into bag if there is a remainder in calculation
    var remainder = pairs_per_board % TILE_LIST.size()
    if remainder != 0:
        for i in range(remainder):
            bag.append(TILE_LIST[i])
            bag.append(TILE_LIST[i])
    bag = Util.shuffle_list(bag)
    return bag
            
    